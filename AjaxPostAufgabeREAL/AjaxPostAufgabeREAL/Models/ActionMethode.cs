﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AjaxPostAufgabeREAL.Models
{
    public class ActionMethode
    {
        public string Eingabe { get; set; }

        public ActionMethode(string eingabe)
        {
            Eingabe = eingabe;
        }

        public string Zählen()
        {
            int buchstaben = 0;
            int wörter = 1;

            foreach (var item in Eingabe)
            {
                buchstaben++;
            }

            foreach (var item in Eingabe)
            {
                if (item is ' ')
                {
                    wörter++;
                }
            }

            return $"Der string {Eingabe} enthält {buchstaben} Buchstaben und {wörter} Wörter";
        }
    }
}