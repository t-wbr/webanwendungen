﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AjaxPostAufgabeREAL.Models;

namespace AjaxPostAufgabeREAL.Controllers
{
    public class HomeController : Controller
    {
        
        // GET: Home
        public ActionResult Index()
        {


            return View();
        }

        [HttpPost]
        public string Zählen(string text)
        {
            int buchstaben = 0;
            int wörter = 1;

            foreach (var item in text)
            {
                buchstaben++;
            }

            foreach (var item in text)
            {
                if (item is ' ')
                {
                    wörter++;
                }
            }

            return $"Der string {text} enthält {buchstaben} Buchstaben und {wörter} Wörter";
        }
    }
}