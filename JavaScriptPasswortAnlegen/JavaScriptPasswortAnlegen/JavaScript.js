﻿window.onload = function () {
    var checkFeld = document.getElementById("wiederholen").onblur = vergleichen;

    var passwortFeld = document.getElementById("passwort");

    var abschicken = document.getElementById("anlegen").onclick = vergleichen;
};

function vergleichen() {

    var passwortFeld = document.getElementById("passwort");
    var checkFeld = document.getElementById("wiederholen");
    var sonderzeichen = new RegExp("[§$%&/(),.;:]")
    var zahl = new RegExp("\\d")
    var großbuchstaben = new RegExp("[A-Z]")

    if (checkFeld.value !== passwortFeld.value) {
        alert("Die Passwörter stimmen nicht überein!");
    }

    else {

        if (checkFeld.value.length >= 8 && zahl.test(checkFeld.value) === true && sonderzeichen.test(checkFeld.value) === true && großbuchstaben.test(checkFeld.value) === true) {
            alert("Das Passwort entspricht den Vorgaben");
        }

        else {
            alert("Das Passwort entspricht nicht den Vorlagen. Es muss mindestens 8 Zeichen lang sein und sollte eine Zahl, ein Sonderzeiche und Großbuchstaben enthalten.");
        }
    }
};



