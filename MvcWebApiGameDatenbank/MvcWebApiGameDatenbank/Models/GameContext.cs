﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MvcWebApiGameDatenbank.Models;

namespace MvcWebApiGameDatenbank.Models
{
    public class GameContext : DbContext
    {
        public DbSet<Result> Results { get; set; }
        public DbSet<Platform> Platforms { get; set; }
        public DbSet<Developer> Developers { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Parent_Platforms> ParentPlatforms { get; set; }



        public GameContext(DbContextOptions options) : base(options)
        {

        }
    }
}
