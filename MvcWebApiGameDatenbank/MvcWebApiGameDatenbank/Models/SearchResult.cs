﻿using System.ComponentModel.DataAnnotations;

namespace MvcWebApiGameDatenbank.Models
{
    public class SearchResult
    {
        public int Id { get; set; }
        public Result[] Results { get; set; }
    }

    public class Result
    {
        public int Id { get; set; }
        public string Slug { get; set; }
        public string Name { get; set; }
        public string Released { get; set; }
        public string Website { get; set; }
        public Parent_Platforms[] Parent_platforms { get; set; }
        public Developer[] Developers { get; set; }
        public string Description_raw { get; set; }
        public Genre[] Genres { get; set; }
    }

    public class Parent_Platforms
    {
        public int Id { get; set; }
        public Platform Platform { get; set; }
    }

    public class Platform
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
    }

    public class Developer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public int Games_count { get; set; }
        public string Image_background { get; set; }
    }

    public class Genre
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
    }
}