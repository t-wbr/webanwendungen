﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using MvcWebApiGameDatenbank.Models;

namespace MvcWebApiGameDatenbank.Models
{
    public class GamesRepository
    {
        private const string url = "https://rawg.io/";

        private readonly HttpClient client;

        public GamesRepository()
        {
            client = new HttpClient();

            client.BaseAddress = new Uri(url);

            client.DefaultRequestHeaders.Clear();

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public SearchResult GetGamebyName(string name)
        {
            HttpResponseMessage responseMessage = client.GetAsync("api/games?search=" + name).Result;

            return DeserializeRequest<SearchResult>(responseMessage);
        }

        public Result GetGamebySlug(string slug)
        {
            HttpResponseMessage responseMessage = client.GetAsync("https://api.rawg.io/api/games/" + slug).Result;

            return DeserializeRequest<Result>(responseMessage);
        }

        private static T DeserializeRequest<T>(HttpResponseMessage responseMessage) where T : class
        {
            // Aufruf erfolgreich?
            if (responseMessage.IsSuccessStatusCode)
            {
                // Antwort der Web-API in einem String speichern
                string result = responseMessage.Content.ReadAsStringAsync().Result;

                // JSON Parsen 
                return JsonConvert.DeserializeObject<T>(result);
            }
            return null;
        }
    }
}
