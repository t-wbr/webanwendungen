﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MvcWebApiGameDatenbank.Models;
using System.Linq;
using System.Threading.Tasks;

namespace MvcWebApiGameDatenbank.Controllers
{
    public class GamesController : Controller
    {
        private readonly GameContext _context;
        private readonly GamesRepository _gRepo;

        public GamesController(GameContext context)
        {
            _context = context;
            _gRepo = new GamesRepository();
        }

        // GET: Games
        public async Task<IActionResult> Index()
        {  
            return View(await _context.Results.ToListAsync());
        }

        // GET: Games/Details/5
        public IActionResult Details(string slug)
        {
            var result =_gRepo.GetGamebySlug(slug);

            return View(result);
        }

        // GET: Games/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Games/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Search(string name)
        {
            return View("Result", _gRepo.GetGamebyName(name));
        }

        // GET: Games/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var searchResult = await _context.Results.FindAsync(id);
            if (searchResult == null)
            {
                return NotFound();
            }
            return View(searchResult);
        }

        // POST: Games/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id")] SearchResult searchResult)
        {
            if (id != searchResult.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(searchResult);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SearchResultExists(searchResult.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(searchResult);
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Save([Bind("Name,Released,Slug")] Result result)
        {            

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Results.Add(result);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SearchResultExists(result.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            return View("Index",await _context.Results.ToListAsync());
        }

        // GET: Games/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var searchResult = await _context.Results
                .FirstOrDefaultAsync(m => m.Id == id);
            if (searchResult == null)
            {
                return NotFound();
            }

            return View(searchResult);
        }

        // POST: Games/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var searchResult = await _context.Results.FindAsync(id);
            _context.Results.Remove(searchResult);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SearchResultExists(int id)
        {
            return _context.Results.Any(e => e.Id == id);
        }
    }
}
